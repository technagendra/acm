import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {DropdownModule} from 'primeng/primeng';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { FooterComponent } from './footer.component';
import { RegisterationComponent } from './registeration.component';
import { ThankyouComponent } from './thankyou.component';
import { ContactusComponent } from './contactus.component';
import { FieldErrorDisplayComponent } from './field-error-display.component';
import { SomthingwentwrongComponent } from './somthingwentwrong.component';
import { AppRoutingModule }     from './app-routing.module';
import { VideoComponent } from './video.component';
import { ClassroomComponent } from './classroom.component';
import {StudentRegistrationComponent} from './student/student-registration.component';
import {TrainerRegistrationComponent} from './trainer/trainer-registration.component';
import {ChangePasswordComponent} from './common/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterationComponent,
    ThankyouComponent,
    ContactusComponent,
    FieldErrorDisplayComponent,
    SomthingwentwrongComponent,
    VideoComponent,
    ClassroomComponent,
    StudentRegistrationComponent,
    TrainerRegistrationComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule ,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

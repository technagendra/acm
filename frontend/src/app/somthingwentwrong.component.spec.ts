import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SomthingwentwrongComponent } from './somthingwentwrong.component';

describe('SomthingwentwrongComponent', () => {
  let component: SomthingwentwrongComponent;
  let fixture: ComponentFixture<SomthingwentwrongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SomthingwentwrongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SomthingwentwrongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

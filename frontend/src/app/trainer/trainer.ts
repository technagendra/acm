export class Trainer {
    constructor() { 
    }
    name: string;
    email:string;
    mobile:string
    address:string
    state:any
    gender:string;    
    university:any;
    institution:String;
    comments:String;    
    otherQualification:String;
    qualification:any;
    otherUniversity:String;
    status:String;
    expertise:String;
    channel : String;
    role:String;

 } 

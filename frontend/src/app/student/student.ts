import {GenericMap} from '../GenericMap';
export class Student {
    constructor() { 
    }
    name: string;
    email:string;
    mobile:string
    address:string
    state:any
    gender:string;
    skill:String;
    university:any;
    institution:String;
    comments:String;
    otherSkill:String;
    otherQualification:String;
    qualification:any;
    traingsRequired:String;
    otherUniversity:String;
    status:String;
    role:String;
    channel:String;
 } 

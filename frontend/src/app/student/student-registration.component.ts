import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Skills } from '../Skills';
import { Http, Response } from '@angular/http';
import { Qualifications } from '../Qualifications';
import {Student} from './student';
import {GenericMap} from '../GenericMap';
import { Observable } from 'rxjs/Observable';
import {Router,RouterLink} from '@angular/router';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Component({
selector : 'student-register',
templateUrl : './student-registration.component.html',
styleUrls :['./student-registration.css' ]
})
export class StudentRegistrationComponent implements OnInit {
private skillsURL = '/api/skills';
private qualificationURL = '/api/qualifications';
private _statesURL = '/api/states';
private _universitiesURL = '/api/universities';
private _saveStudentURL='/api/saveStudent';

form : FormGroup;
qualifications : Qualifications[];
states: GenericMap[];
universities : GenericMap[];
isUniversityOthers : boolean = false;
isQualificationOthers :boolean =  false;
student : Student = new Student();
constructor(private http: Http, private formBuilder: FormBuilder, private router: Router) { 
  }



ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email,Validators.minLength(6)]),
      'name': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'gender': new FormControl(null, [Validators.required]),
      'mobile': new FormControl(null, [Validators.required,Validators.minLength(10)]),
      'address': new FormControl(null, [Validators.required,Validators.minLength(4)]),     
      'state': new FormControl(null, [Validators.required]),
      'university':new FormControl(null, [Validators.required]),
      'institution': new FormControl(null, [Validators.required]),
      'comments': new FormControl(null, []),
      'qualification': new FormControl(null, [Validators.required]),
      'traingsRequired': new FormControl(null, [Validators.required]),
      'otherQualification': new FormControl(null, []),
      'otherUniversity': new FormControl(null, []),         
    });
    
    
    this.fetchQualifications();
    this.fetchStates();
    this.fetchUniversities();
  }

isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  enroll() {   
    if(this.form.status === "INVALID"){
      for(let i in this.form.controls){      
      if((!this.isQualificationOthers && this.form.controls.otherQualification == this.form.controls[i]) || 
      (!this.isUniversityOthers && this.form.controls.otherUniversity == this.form.controls[i])) {
        continue;
      } else {
        this.form.controls[i].markAsTouched();
      }
    } 

    } else {
      
      console.log("Before ::"+JSON.stringify(this.student));
      if(this.student.university && this.student.university.key === 'Others'){
        this.student.university = this.student.otherUniversity;
      } else {
        this.student.university = this.student.university.key;
      }
      
      if(this.student.qualification && this.student.qualification.code === 'Others') {
        this.student.qualification = this.student.otherQualification;
      } else {
        this.student.qualification = this.student.qualification.code;
      }
      this.student.state=this.student.state.key;
      console.log(JSON.stringify(this.student));
      this.student.status = 'OPEN'
      this.student.role = 'STUDENT';
      this.student.channel='WEB'
      this.saveStudent(this.student);


    }    
  }

saveStudent(student: Student){
return this.http.post(this._saveStudentURL, this.student)
.map((res)=> { 
  let data= res.json();
  if(data){
    this.router.navigate(['/thankyou/'+data.refId]);    
  } 
  })
.subscribe(
 (data:any)=> {
  console.log(data)
 },
 (error)=>this.router.navigate(['/error'])   
)
}


  fetchQualifications(){
      return this.http.get(this.qualificationURL)
      .map(res => res.json())
      .subscribe(
          (qualifications:Qualifications[])=>this.qualifications = qualifications,
          (error)=>console.log(error)
      )
  }

  changeUniversity(selectedUniversity){    
    if(selectedUniversity && selectedUniversity.value === 'Others'){
      this.isUniversityOthers = true;
      this.form.get('otherUniversity').setValidators([Validators.required]);
    } else {
        this.isUniversityOthers = false;
    }
  }
  changeQualification(qualifiactionSelected) {
      debugger;
     
    if(qualifiactionSelected && qualifiactionSelected.name === "Others"){
        this.isQualificationOthers = true;
        this.form.get('otherQualification').setValidators([Validators.required]);
         
    } else {
        this.isQualificationOthers = false;
    }
  }

fetchStates() {
return this.http.get(this._statesURL)
.map(res => res.json())
.subscribe(
  (states:GenericMap[])=> this.states = states,
  (error)=>console.log(error)
)
  }

  fetchUniversities(){
    return this.http.get(this._universitiesURL)
    .map(res => res.json())
    .subscribe(
      (universities:GenericMap[]) => this.universities = universities,
      (error)=>console.log(error)
    )
  }
 
}

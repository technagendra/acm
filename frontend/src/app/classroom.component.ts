import { Component, OnInit, ViewEncapsulation, HostListener, Input } from '@angular/core';
import { OpenVidu, Session, Stream } from 'openvidu-browser';
import { Http, Response } from '@angular/http';
import { VideoSession } from './VideoSession';

@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ClassroomComponent implements OnInit {

  private sessionURL = '/api/startClassRoom';

  ngOnInit() {
  }
  // OpenVidu objects
  OV: OpenVidu;
  session: Session;

  // Streams to feed StreamComponent's
  remoteStreams: Stream[] = [];
  localStream: Stream;

  // Join form
  sessionId: string;
  userName: string;

  // Main video of the page, will be 'localStream' or one of the 'remoteStreams',
  // updated by an Output event of StreamComponent children
  @Input() mainVideoStream: Stream;

  constructor(private http: Http) {

  }

  @HostListener('window:beforeunload')
  beforeunloadHandler() {
    // On window closed leave session
    this.leaveSession();
  }

  ngOnDestroy() {
    // On component destroyed leave session
    this.leaveSession();
  }

  joinSession = () => {

    this.http.get(this.sessionURL)
      .map(res => res.json())
      .subscribe(
      (session: VideoSession) => {
        this.OV = new OpenVidu();
        this.session = this.OV.initSession(session.sessionId);

        this.session.on('streamCreated', (event) => {
          this.remoteStreams.push(event.stream);
          this.session.subscribe(event.stream, '');
        });
        this.session.on('streamDestroyed', (event) => {
          event.preventDefault();
          this.deleteRemoteStream(event.stream);
        });
        this.session.connect(null, '{"clientData": "' + this.userName + '"}', (error) => {
          if (!error) {
            let publisher = this.OV.initPublisher('', {
              audio: true,        // Whether you want to transmit audio or not
              video: true,        // Whether you want to transmit video or not
              audioActive: true,  // Whether you want to start the publishing with your audio unmuted or muted
              videoActive: true,  // Whether you want to start the publishing with your video enabled or disabled
              quality: 'MEDIUM',  // The quality of your video ('LOW', 'MEDIUM', 'HIGH')
              screen: false       // true to get your screen as video source instead of your camera
            });
            this.localStream = publisher.stream;
            this.mainVideoStream = this.localStream;
            this.session.publish(publisher);

          } else {
            console.log('There was an error connecting to the session:', error.code, error.message);
          }
        });

      },
      (error) => console.log(error)
      )
  }

  leaveSession() {
    if (this.session) { this.session.disconnect(); };
    this.remoteStreams = [];
    this.localStream = null;
    this.session = null;
    this.OV = null;
  }


  private deleteRemoteStream(stream: Stream): void {
    let index = this.remoteStreams.indexOf(stream, 0);
    if (index > -1) {
      this.remoteStreams.splice(index, 1);
    }
  }

  private getMainVideoStream(stream: Stream) {
    this.mainVideoStream = stream;
  }
}

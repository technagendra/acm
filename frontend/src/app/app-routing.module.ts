import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {RegisterationComponent} from './registeration.component'
import {ThankyouComponent} from './thankyou.component'
import {SomthingwentwrongComponent} from './somthingwentwrong.component'
import {StudentRegistrationComponent} from './student/student-registration.component';
import { TrainerRegistrationComponent } from './trainer/trainer-registration.component';
import { ChangePasswordComponent } from './common/change-password.component';
import {ClassroomComponent} from './classroom.component';


const routes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'register', component: RegisterationComponent },
  { path: 'thankyou/:refId', component: ThankyouComponent },
  { path: 'error', component: SomthingwentwrongComponent },
  { path: 'studentreg', component: StudentRegistrationComponent },
  { path: 'trainerreg', component: TrainerRegistrationComponent},
  { path: 'changepassword', component: ChangePasswordComponent},
  { path: 'classroom',component:ClassroomComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}

export class ChangePassword {

    constructor(
        public username: string,
        public newpassword : string,
        public confirmpassword : string,){        
    }

}
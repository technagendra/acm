import { Component, OnInit } from '@angular/core';
import { ChangePassword } from './changepassword';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response } from '@angular/http';
import {Router,RouterLink} from '@angular/router';

@Component({
    selector : 'change-password',
    templateUrl : './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

changepassword : ChangePassword = new ChangePassword('','','');
form : FormGroup;
isEqual: boolean = false;

constructor(private http : Http, private formBuilder : FormBuilder, private router : Router ) {
    this.form = new FormGroup({
    'username' : new FormControl(null, [Validators.required]),    
    'newpassword': new FormControl(null, [Validators.required]),
    'confirmpassword': new FormControl(null, [Validators.required])
    });   
}

ngOnInit() {
this.form = new FormGroup({
    'username' : new FormControl(null, [Validators.required]),
    'newpassword': new FormControl(null, [Validators.required]),
    'confirmpassword': new FormControl(null, [Validators.required])
})
}

changePassword() {
    if(this.form.status === "INVALID") {
        for(let i in this.form.controls) {         
            this.form.controls[i].markAsTouched();      
        }        
    } else {
        console.log(this.form.controls);
    }        
}


isFieldValid(field: string){
    return !this.form.get(field).valid && this.form.get(field).touched;
}

isPasswordEqual(field: string) {  
      if(!this.isFieldValid(field)) {
            return (this.form.get('confirmpassword').dirty && this.form.get('newpassword').dirty) &&(this.form.get('confirmpassword').value !== this.form.get('newpassword').value)
      }   
}


displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
}

}
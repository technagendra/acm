import { Component,ViewEncapsulation, OnInit, Input, Output, AfterViewInit, DoCheck, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Stream } from 'openvidu-browser';

@Component({
  selector: 'video-component',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VideoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @ViewChild('videoElement') elementRef: ElementRef;

    videoElement: HTMLVideoElement;

    @Input()
    stream: Stream;

    @Input()
    isMuted: boolean;

    @Output()
    mainVideoStream = new EventEmitter();

    ngAfterViewInit() { // Get HTMLVideoElement from the view
        this.videoElement = this.elementRef.nativeElement;
    }

    ngDoCheck() { // Detect any change in 'stream' property (specifically in its 'srcObject' property)
        if (this.videoElement && (this.videoElement.srcObject !== this.stream.getVideoSrcObject())) {
            this.videoElement.srcObject = this.stream.getVideoSrcObject();
        }
    }

    getNicknameTag() { // Gets the nickName of the user
        return JSON.parse(this.stream.connection.data).clientData;
    }

    videoClicked() { // Triggers event for the parent component to update its view
        this.mainVideoStream.next(this.stream);
    }
}

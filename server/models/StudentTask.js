const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const User = require('../models/User');
autoIncrement.initialize(mongoose.connection); 
var Schema = mongoose.Schema;


const studentTaskSchema = new mongoose.Schema({
  userId: { type: Schema.Types.ObjectId, required: true,ref:'User'},
  trainerId: { type: Schema.Types.ObjectId,ref:'User'},
  traingsRequired: { type: String, required: true },
  paymentMade: { type: Boolean},
  comments:{ type: String },
  paymentDate: { type: Date },
  payemntDetails: { type: String },
  course: { type: String },
  std_refNo:{ type: String, unique: true },
  updatedOn:{ type: String},
  updatedBy:{ type: String},
  status:{ type: String,required: true},
  channel:{ type: String}
}, { timestamps: true });

studentTaskSchema.plugin(autoIncrement.plugin,  {model: 'studentTaskSchema', field: 'std_refNo', startAt: 100001, incrementBy: 1});
var StudentTask = mongoose.model("StudentTask", studentTaskSchema);
module.exports = StudentTask;



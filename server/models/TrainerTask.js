const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const User = require('../models/User');
autoIncrement.initialize(mongoose.connection); 
var Schema = mongoose.Schema;


const trainerTaskSchenma = new mongoose.Schema({
  userId: { type: Schema.Types.ObjectId, required: true,ref:'User'},  
  expertise : { type: String, require: true },
  comments:{ type: String },  
  tr_refNo:{ type: String, unique: true },
  updatedOn:{ type: String},
  updatedBy:{ type: String},
  status:{ type: String,required: true},
  channel:{ type: String}
}, { timestamps: true });

trainerTaskSchenma.plugin(autoIncrement.plugin,  {model: 'trainerTaskSchenma', field: 'tr_refNo', startAt: 100001, incrementBy: 1});
var TrainerTask = mongoose.model("trainerTask", trainerTaskSchenma);
module.exports = TrainerTask;



const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const qualificationsSchema = new mongoose.Schema({
  name: { type: String, unique: true ,required: true},
  code: { type: String, unique: true ,required: true}
});
var Qualification = mongoose.model("qualification", qualificationsSchema);
module.exports = Qualification;



const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true ,required: true},
  name: { type: String, required: true },
  mobile:{ type: String, required: true },
  gender: { type: String, required: true },
  address: { type: String, required: true },  
  qualification: { type: String, required: true },
  university: { type: String, required: true },
  institution: { type: String, required: true },
  state: { type: String, required: true },
  comments: { type: String },
  
  role: { type: String, required: true},
}, { timestamps: true });

var User = mongoose.model("User", userSchema);
module.exports = User;



const bluebird = require('bluebird');
const crypto = bluebird.promisifyAll(require('crypto'));

/* CONFIGURATION */
var OpenVidu = require('openvidu-node-client').OpenVidu;
var Session = require('openvidu-node-client').Session;
var OpenViduRole = require('openvidu-node-client').OpenViduRole;
var TokenOptions = require('openvidu-node-client').TokenOptions;

var OV = new OpenVidu(process.env.OPENVIDU_URL, process.env.OPENVIDU_SESSION_SECRET);
var mapSessionNameSession = {};
var mapSessionIdTokens = {};

exports.startClassRoom = (req, res, next) => {
    var mySession = OV.createSession();
    //TODO :change with user data from session
    const serverData = { name: 'Test_User' };
    const role = 'PUBLISHER'

    var tokenOptions = new TokenOptions.Builder()
        .data({ serverData })
        .role(role)
        .build();
    //TODO: change session_id with unique id i.e classroom with trainer task id
    if (mapSessionNameSession['classroom']) {
        console.log('Existing session ' + sessionName);
        var mySession = mapSessionNameSession[sessionName];
        
        mySession.generateToken(tokenOptions, function (token) {
            mySession.getSessionId(function (sessionId) {
                mapSessionIdTokens[sessionId].push(token);
                console.log('SESSIONID: ' + sessionId);
                console.log('TOKEN: ' + token);
                res.status(200).send({
                    'sessionId': sessionId,
                    'token': token
                });
            });
        });
    }
    else {
        mySession.getSessionId(function (sessionId) {
            //TODO: change session_id with unique id i.e classroom with trainer task id
            mapSessionNameSession['classroom'] = mySession;
            mapSessionIdTokens[sessionId] = [];
            mySession.generateToken(tokenOptions, function (token) {
                mapSessionIdTokens[sessionId].push(token);
                console.log('SESSIONID: ' + sessionId);
                console.log('TOKEN: ' + token);
                res.status(200).send({
                    'sessionId': sessionId,
                    'token': token
                });
            });
        });
    }
};


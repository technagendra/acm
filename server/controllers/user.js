const bluebird = require('bluebird');
const crypto = bluebird.promisifyAll(require('crypto'));
const nodemailer = require('nodemailer');
const passport = require('passport');
const User = require('../models/User');
const Skills = require('../models/Skills');
const Qualification = require('../models/Qualification');
const StudentTask = require('../models/StudentTask');
const TrainerTask = require('../models/TrainerTask')

exports.register = (req, res, next) => {
  console.log(req.body);
  var user = new User(req.body);
  try {
    user.save()
      .then(userObj => {
        try {
          sendMail(userObj);
          console.log(user.email + " Success");
        } catch (err) {
          console.log(err);
        };
        res.send({ msg: "sucess", refNo: "MN" + userObj.refNo });
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({ msg: "error" });
      });
  } catch (error) {
    console.log(error);
    res.status(500).send({ msg: "error" });
  }
};

exports.saveStudent = (req, res, next) => {
  console.log(req.body);
  var user = new User(req.body);
  var studentTask = new StudentTask(req.body);
  studentTask.userId = user._id;

  console.log("user data===" + JSON.stringify(user));
  console.log("Student data===" + JSON.stringify(studentTask));
  try {
    user.save()
      .then(userObj => {
        studentTask.save()
          .then(taskObj => {
            console.log("Student task saved successfully STD" + taskObj.std_refNo)
            let refNo = "STD" + taskObj.std_refNo
            sendMail(userObj, refNo);
            res.status(200).send({ msg: "sucess", refId: refNo });
          })
          .catch(err => {
            console.log(err);
            res.status(500).send({ msg: "error" });
          });
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({ msg: "error" });
      });
  } catch (error) {
    console.log(error);
    res.status(500).send({ msg: "error" });
  }
};

exports.saveTrainer = (req, res, next) => {
  debugger;
  var user = new User(req.body);
  var trainerTask = new TrainerTask(req.body);
  trainerTask.userId = user._id;
  console.log('User trainer :: ' + JSON.stringify(user));
  console.log('Trainer data :: ' + JSON.stringify(trainerTask));
  try {
    user.save()
      .then(
      (userObj) => {
        trainerTask.save()
          .then(trainerObj => {
            console.log("Trainer task saved successfully TR" + trainerObj.tr_refNo);
            let refNo = "TR" + trainerObj.tr_refNo;

            sendMail(userObj, refNo);
            console.log("refNo--------" + refNo);
            res.status(200).send({ msg: "sucess", refId: refNo });
          })
          .catch(err => {
            console.log('Error while saving Trainer Task' + err);
            res.status(500).send({ msg: "error" });
          });
      }).catch(err => {
        console.log(err);
      });
  } catch (error) {
    console.log(error);
    res.status(500).send({ msg: "error" });
  }
}

exports.skills = (req, res, next) => {
  Skills.find({}, { _id: 0 }).exec(function (err, docs) {
    if (!err) {
      console.log(docs);
      res.send(docs);
    } else {
      res.send(err);
    }
  });
};
exports.qualifications = (req, res, next) => {
  //debugger;
  console.log("I m at Q");
  Qualification.find({}, { _id: 0 }).exec(function (err, docs) {
    if (!err) {
      res.send(docs);
    } else {
      res.send(err);
    }
  });
};

exports.states = (req, res, next) => {
  console.log('inside states');
  var testdata = [
    {
      "key": "Andhra Pradesh",
      "value": "Andhra Pradesh"
    },
    {
      "key": "Telangana",
      "value": "Telangana"
    },
    {
      "key": "Karnataka",
      "value": "Karnataka"
    }
  ];
  res.send(testdata);
}

exports.universities = (req, res, next) => {
  console.log('inside states');
  var universities = [
    {
      "key": "JNTU",
      "value": "JNTU"
    },
    {
      "key": "Osmania",
      "value": "Osmania"
    },
    {
      "key": "Kakatiya",
      "value": "Kakatiya"
    },
    {
      "key": "Nagarjuna",
      "value": "Nagarjuna"
    },
    {
      "key": "Others",
      "value": "Others"
    }
  ];
  res.send(universities);
}
const sendMail = (user, refNo) => {
  if (!user || !refNo) { return; }
  const transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
      user: process.env.SENDGRID_USER,
      pass: process.env.SENDGRID_PASSWORD
    }
  });
  const data = {
    to: user.email,
    from: 'info@promentorship.com',
    subject: 'Mentorship Enrollment Confirmation',
    text: `Hello ${user.name},\n\n\n We appreciate your intrest in Mentorship.\n\nHere is the referene id : MN${refNo} for future refernce.\n\nWe will get back to you soon. \n\n\nRegards \n\n Mentorship Team`
  };
  transporter.sendMail(data)
    .then(() => {
      console.log("E-Mail Sent Successfully To " + user.email);
    }).catch((error) => {
      console.log("Email: ", error);
    })
};
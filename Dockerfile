FROM node:6-slim
COPY . /mentorship
WORKDIR /mentorship/server
RUN npm rebuild node-sass --force
CMD ["npm","start"]
EXPOSE 3000
